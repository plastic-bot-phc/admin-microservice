import { ChannelConfigDTO } from '@dtos/channel-config';
import { SetChannelConfigDTO } from '@dtos/set-channel-config';
import { ChannelType } from '@enums/channel-type';
import { Injectable } from '@nestjs/common';
import { ChannelConfigRepository } from '@repositories/channel-config';
import { ChannelConfigDocument } from '@schemas/channel-config';

@Injectable()
export class ChannelConfigService {
  constructor(private readonly repository: ChannelConfigRepository) {}

  getConfessionChannelConfig(guild: string): Promise<ChannelConfigDTO | null> {
    return this.getChannelConfig(guild, ChannelType.CONFESSION);
  }

  setConfessionChannelConfig(data: SetChannelConfigDTO): Promise<void> {
    return this.setChannelConfig(data, ChannelType.CONFESSION);
  }

  getTwitchChannelConfig(guild: string): Promise<ChannelConfigDTO | null> {
    return this.getChannelConfig(guild, ChannelType.TWITCH);
  }

  setTwitchChannelConfig(data: SetChannelConfigDTO): Promise<void> {
    return this.setChannelConfig(data, ChannelType.TWITCH);
  }

  private async getChannelConfig(
    guild: string,
    type: ChannelType,
  ): Promise<ChannelConfigDTO | null> {
    const document: ChannelConfigDocument =
      await this.repository.findOneByGuildAndType(guild, type);
    return (document?.toJSON() as ChannelConfigDTO) || null;
  }

  private async setChannelConfig(
    data: SetChannelConfigDTO,
    type: ChannelType,
  ): Promise<void> {
    const current: ChannelConfigDocument | null =
      await this.repository.findOneByGuildAndType(data.guild, type);

    if (current !== null) {
      current.channel = data.channel;
      await current.save();
    } else {
      await this.repository.save({
        ...data,
        type,
      });
    }
  }
}
