import { ChannelType } from '@enums/channel-type';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { ChannelConfig, ChannelConfigDocument } from '@schemas/channel-config';
import { Model } from 'mongoose';

@Injectable()
export class ChannelConfigRepository {
  constructor(
    @InjectModel(ChannelConfig.name)
    private readonly model: Model<ChannelConfig>,
  ) {}

  findOneByGuildAndType(
    guild: string,
    type: ChannelType,
  ): Promise<ChannelConfigDocument | null> {
    return this.model.findOne({ guild, type }).exec();
  }

  save(data: ChannelConfig): Promise<ChannelConfigDocument> {
    return new this.model(data).save();
  }
}
