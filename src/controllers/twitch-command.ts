import {
  GET_TWITCH_CHANNEL_PATTERN,
  SET_TWITCH_CHANNEL_PATTERN,
} from '@constants/pattern';
import { ChannelConfigDTO } from '@dtos/channel-config';
import { GetChannelConfigDTO } from '@dtos/get-channel-config';
import { SetChannelConfigDTO } from '@dtos/set-channel-config';
import { Controller, Logger } from '@nestjs/common';
import { MessagePattern, Payload } from '@nestjs/microservices';
import { ChannelConfigService } from '@services/channel-config';

@Controller()
export class TwitchCommandController {
  private readonly logger: Logger = new Logger(TwitchCommandController.name);

  constructor(private readonly channelConfigService: ChannelConfigService) {}

  @MessagePattern(GET_TWITCH_CHANNEL_PATTERN)
  async getTwitchChannel(
    @Payload() data: GetChannelConfigDTO,
  ): Promise<string> {
    this.logger.log('getTwitchChannel', data);

    const channelConfig: ChannelConfigDTO | null =
      await this.channelConfigService.getTwitchChannelConfig(data.guild);
    return channelConfig?.channel;
  }

  @MessagePattern(SET_TWITCH_CHANNEL_PATTERN)
  async setTwitchChannel(@Payload() data: SetChannelConfigDTO): Promise<void> {
    this.logger.log('setTwitchChannel', data);

    await this.channelConfigService.setTwitchChannelConfig(data);
  }
}
