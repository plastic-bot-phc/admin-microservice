import {
  GET_CONFESSION_CHANNEL_PATTERN,
  SET_CONFESSION_CHANNEL_PATTERN,
} from '@constants/pattern';
import { ChannelConfigDTO } from '@dtos/channel-config';
import { GetChannelConfigDTO } from '@dtos/get-channel-config';
import { SetChannelConfigDTO } from '@dtos/set-channel-config';
import { Controller, Logger } from '@nestjs/common';
import { MessagePattern, Payload } from '@nestjs/microservices';
import { ChannelConfigService } from '@services/channel-config';

@Controller()
export class ConfessionCommandController {
  private readonly logger: Logger = new Logger(
    ConfessionCommandController.name,
  );

  constructor(private readonly channelConfigService: ChannelConfigService) {}

  @MessagePattern(GET_CONFESSION_CHANNEL_PATTERN)
  async getConfessionChannel(
    @Payload() data: GetChannelConfigDTO,
  ): Promise<string> {
    this.logger.log('getConfessionChannel', data);

    const channelConfig: ChannelConfigDTO | null =
      await this.channelConfigService.getConfessionChannelConfig(data.guild);
    return channelConfig?.channel;
  }

  @MessagePattern(SET_CONFESSION_CHANNEL_PATTERN)
  async setConfessionChannel(
    @Payload() data: SetChannelConfigDTO,
  ): Promise<void> {
    this.logger.log('setConfessionChannel', data);

    await this.channelConfigService.setConfessionChannelConfig(data);
  }
}
