export const SET_CONFESSION_CHANNEL_PATTERN = 'admin:set-confession-channel';

export const GET_CONFESSION_CHANNEL_PATTERN = 'admin:get-confession-channel';

export const SET_TWITCH_CHANNEL_PATTERN = 'admin:set-twitch-channel';

export const GET_TWITCH_CHANNEL_PATTERN = 'admin:get-twitch-channel';
