import { ChannelType } from '@enums/channel-type';
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type ChannelConfigDocument = ChannelConfig & Document;

@Schema({
  collection: 'channels_config',
  timestamps: true,
})
export class ChannelConfig {
  @Prop({
    required: true,
  })
  guild: string;

  @Prop({
    required: true,
    enum: Object.values(ChannelType),
  })
  type: ChannelType;

  @Prop({
    required: true,
  })
  channel: string;
}

export const ChannelConfigSchema = SchemaFactory.createForClass(ChannelConfig);
