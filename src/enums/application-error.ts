export enum ApplicationError {
  CONFIGURATION_NOT_FOUND = 'configuration-not-found-error',
  UNKNOWN = 'unknown-error',
}
