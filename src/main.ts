import { REDIS_PASSWORD_ENV, REDIS_URI_ENV } from '@constants/environment';
import { NestFactory } from '@nestjs/core';
import { MicroserviceOptions, Transport } from '@nestjs/microservices';
import { AppModule } from './app.module';

async function bootstrap() {
  const app = await NestFactory.createMicroservice<MicroserviceOptions>(
    AppModule,
    {
      transport: Transport.REDIS,
      options: {
        host: process.env[REDIS_URI_ENV],
        ...(process.env[REDIS_PASSWORD_ENV]
          ? { password: process.env[REDIS_PASSWORD_ENV] }
          : {}),
      },
    },
  );

  app.listen();
}

bootstrap();
