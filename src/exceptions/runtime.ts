import { RpcException } from '@nestjs/microservices';

export class RuntimeException extends RpcException {}
