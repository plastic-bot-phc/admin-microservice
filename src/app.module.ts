import { MONGO_URI_ENV } from '@constants/environment';
import { ConfessionCommandController } from '@controllers/confession-command';
import { TwitchCommandController } from '@controllers/twitch-command';
import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { MongooseModule } from '@nestjs/mongoose';
import { ChannelConfigRepository } from '@repositories/channel-config';
import { ChannelConfig, ChannelConfigSchema } from '@schemas/channel-config';
import { ChannelConfigService } from '@services/channel-config';

@Module({
  imports: [
    ConfigModule.forRoot(),
    MongooseModule.forRootAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: (configService: ConfigService) => ({
        uri: configService.get(MONGO_URI_ENV),
      }),
    }),
    MongooseModule.forFeature([
      { name: ChannelConfig.name, schema: ChannelConfigSchema },
    ]),
  ],
  controllers: [ConfessionCommandController, TwitchCommandController],
  providers: [ChannelConfigService, ChannelConfigRepository],
})
export class AppModule {}
