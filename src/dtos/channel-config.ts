import { ChannelType } from '@enums/channel-type';

export class ChannelConfigDTO {
  _id: string;
  guild: string;
  type: ChannelType;
  channel: string;
}
