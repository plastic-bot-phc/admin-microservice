export class SetChannelConfigDTO {
  guild: string;
  channel: string;
}
